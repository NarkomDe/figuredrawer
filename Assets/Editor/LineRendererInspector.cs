﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(LineRenderer))]
[CanEditMultipleObjects]
public class LineRendererInspector : Editor
{
    SerializedProperty materials;
    SerializedProperty positions;
    SerializedProperty parameters;
    SerializedProperty useWorldSpace;

    void OnEnable()
    {
        LineRenderer r = (LineRenderer)target;
        r.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        r.receiveShadows = false;
        r.useLightProbes = false;
        r.reflectionProbeUsage = UnityEngine.Rendering.ReflectionProbeUsage.Off;
        r.probeAnchor = null;
        r.lightmapIndex = 0;

        materials = serializedObject.FindProperty("m_Materials");
        positions = serializedObject.FindProperty("m_Positions");
        parameters = serializedObject.FindProperty("m_Parameters");
        useWorldSpace = serializedObject.FindProperty("m_UseWorldSpace");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUI.BeginChangeCheck();
        EditorGUILayout.PropertyField(materials, true);
        EditorGUILayout.PropertyField(positions, true);
        EditorGUILayout.PropertyField(parameters, true);
        EditorGUILayout.PropertyField(useWorldSpace, true);

        if (EditorGUI.EndChangeCheck())
            serializedObject.ApplyModifiedProperties();
    }
}
