﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "Shape", menuName = "Pattern shape", order = 1)]
public class PatternShape : ScriptableObject {
    public Vector2[] points;
}
