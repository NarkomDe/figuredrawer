﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PatternViewerScript : MonoBehaviour 
{
    public PatternShape shape;
    public GameObject segmentPrefab;
    public GameObject jointPrefab;

    [HideInInspector]
    public Transform segments;
    [HideInInspector]
    public Transform joints;
    [HideInInspector]
    public List<Vector2> edges;

    void Awake()
    {
        segments = transform.GetChild(0);
        joints = transform.GetChild(1);
        edges = new List<Vector2>();
    }

	void Start() 
	{
        Initialize();
	}

	public void Initialize() 
	{
        Vector2[] points = shape.points;

        edges.Clear();
        for (int i = 0; i < points.Length; i++)
            edges.Add(points[(i + 1) % points.Length] - points[i]);

        //only for figures with over 10 points one in game
        while (segments.childCount < points.Length)
        {
            GameObject inst = Instantiate<GameObject>(segmentPrefab);
            inst.transform.parent = segments;
            inst.transform.position = new Vector3(0, 0, 2);
        }

        while (joints.childCount < points.Length)
        {
            GameObject inst = Instantiate<GameObject>(jointPrefab);
            inst.transform.parent = joints;
            inst.transform.position = new Vector3(0, 0, 2);
        }

        for (int i = 0; i < segments.childCount; i++)
        {
            Transform segment = segments.GetChild(i);

            if (i < points.Length)
            {
                segment.gameObject.SetActive(true);
                InitializeSegment(segment, points[i], points[(i + 1) % points.Length]);
            }
            else
                segment.gameObject.SetActive(false);
        }

        for (int i = 0; i < joints.childCount; i++)
        {
            Transform joint = joints.GetChild(i);

            if (i < points.Length)
            {
                joint.gameObject.SetActive(true);
                joint.position = new Vector3(points[i].x, points[i].y, joint.position.z);
            }
            else
                joint.gameObject.SetActive(false);
        }
	}

    void InitializeSegment(Transform segment, Vector2 a, Vector2 b)
    {
        segment.position = new Vector3(((a + b) / 2).x, ((a + b) / 2).y, segment.position.z);
        segment.localScale = new Vector3(segment.localScale.x, (a - b).magnitude, segment.localScale.z);
        segment.rotation = Quaternion.Euler(new Vector3(0, 0, Mathf.Rad2Deg * Mathf.Atan2(a.y - b.y, a.x - b.x) - 90));
    }
}
