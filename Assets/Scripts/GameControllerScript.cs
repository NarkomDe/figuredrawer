﻿using UnityEngine;
using System.Collections;

public class GameControllerScript : MonoBehaviour {
    public GameObject startCanvas;
    public GameObject endCanvas;
    public GameObject gameInfo;
    public DrawLineGeneratorScript drawer;
    public PatternViewerScript patternViewer;
    public TimerScript timer;
    public ScoreScript result;
    public ScoreScript score;
    public float baseTime;
    public float timeDecreacePerLevel;

    public PatternShape[] levels;

    private int currentLevel;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }

    public void StartGame()
    {
        startCanvas.SetActive(false);
        endCanvas.SetActive(false);

        drawer.gameObject.SetActive(true);
        patternViewer.gameObject.SetActive(true);
        gameInfo.SetActive(true);

        currentLevel = 0;
        SetLevel(currentLevel);
    }

    public void NextLevel()
    {
        SetLevel(++currentLevel);
    }

    public void EndGame()
    {
        endCanvas.SetActive(true);

        drawer.Restart();
        drawer.gameObject.SetActive(false);
        patternViewer.gameObject.SetActive(false);
        gameInfo.SetActive(false);

        result.SetScore(currentLevel);
    }

    void SetLevel(int level)
    {
        patternViewer.shape = levels[level % levels.Length];
        patternViewer.Initialize();
        timer.SetTimer(baseTime - timeDecreacePerLevel * currentLevel);
        score.SetScore(currentLevel);
    }
}
