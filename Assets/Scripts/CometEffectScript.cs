﻿using UnityEngine;
using System.Collections;

public class CometEffectScript : MonoBehaviour 
{
    public DrawLineGeneratorScript drawer;

    private TrailRenderer trailRenerer;

    void Awake()
    {
        trailRenerer = GetComponent<TrailRenderer>(); 
    }

    void Update()
    {
        if (drawer.isDrawing)
        {
            trailRenerer.enabled = true;

            Vector3 newPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            newPosition.z = -.1f;
            transform.position = newPosition;
        }
    }
}
