﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PatternCheckerScript : MonoBehaviour 
{
    public PatternViewerScript viewer;
    public LitManagerScript litManager;
    
    [Range(0, 60)]
    public float maxAngleDifference = 30;
    public float minInitializeDistance = .2f;
    public float maxDistanceDevAtStart = 15;
    public float maxDistanceDevAtEnd;
    public float minDistanceToStopDevGrow;
    public float minEdgeLenght = .5f;
    public float maxDistanceBetweenEnds;

    [Range(1, 4)]
    public float maxDistanceDeviationUp;
    [Range(0, 1)]
    public float maxDistanceDeviationDown;

    private List<Match> matches;
    private List<Match> forRemove;
    private bool isInit;


    void Awake()
    {
        matches = new List<Match>();
        forRemove = new List<Match>();
        isInit = false;
    }

    public void Restart()
    {
        matches.Clear();
        isInit = false;
        litManager.LitEdges(matches);
    }

    public bool IsFinished()
    {
        for (int i = 0; i < matches.Count; i++)
            if (matches[i].IsFinished())
                return true;

        return false;
    }

    public bool CheckPattern(List<Vector2> points)
    {
        if (isInit)
        {
            forRemove.Clear();

            for (int i = 0; i < matches.Count; i++)
                if (!(matches[i].Check(points)))
                    forRemove.Add(matches[i]);

            for (int i = 0; i < forRemove.Count; i++)
                matches.Remove(forRemove[i]);

            if (matches.Count == 0)
                return false;
        }
        else
        {
            if ((points[points.Count - 1] - points[0]).magnitude > minInitializeDistance)
            {
                Vector2 direction = points[points.Count - 1] - points[0];
                InitMatches(direction);

                if (matches.Count == 0)
                    return false;

                isInit = true;
            }
        }

        litManager.LitEdges(matches);
        return true;
    }

    void InitMatches(Vector2 direction)
    {
        for (int i = 0; i < viewer.edges.Count; i++)
        {
            float angleDifPos = MinAngleBetween(direction, viewer.edges[i]);
            float angleDifNeg = MinAngleBetween(direction, -viewer.edges[i]);
            float angleDifMin = Mathf.Min(angleDifPos, angleDifNeg);

            if (angleDifMin < maxAngleDifference)
                matches.Add(new Match(i, (angleDifMin == angleDifPos) ? true : false, this));
        }
    }
   
    float MinAngleBetween(Vector2 a, Vector2 b)
    {
        return Mathf.Min(Vector2.Angle(a, b), Vector2.Angle(b, a));
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;

        if (matches != null)
            for (int i = 0; i < matches.Count; i++)
                for (int j = 0; j < matches[i].vertices.Count; j++)
                    Gizmos.DrawSphere(matches[i].vertices[j], .2f);
    }
}
