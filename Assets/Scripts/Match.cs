﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Match
{
    public int currentEdgeIndex;
    public bool directionOfDrawing;
    public List<Vector2> vertices;

    private PatternCheckerScript checker;
    private float correctLengthRelation;
    private Vector2 lastPoint;

    private Vector2 EdgeVector { get {
            return (directionOfDrawing ? 1 : -1) * checker.viewer.edges[currentEdgeIndex];
        }
    }

    public Match(int edge, bool direction, PatternCheckerScript checker)
    {
        currentEdgeIndex = edge;
        directionOfDrawing = direction;
        vertices = new List<Vector2>();
        this.checker = checker;
    }

    public bool Check(List<Vector2> points)
    {
        if (vertices.Count == 0)
            vertices.Add(points[0]);

        if (IsPointInCurrentEdge(points[points.Count - 1]))
        {
            Vector2 lastEdgeVector = EdgeVector;
            SetNextEdge();

            //locating and adding vertex point
            int edgeVertex = points.Count - 2;
            while (MinAngleBetween(points[edgeVertex + 1] - points[edgeVertex], EdgeVector) <
                MinAngleBetween(points[edgeVertex + 1] - points[edgeVertex], lastEdgeVector) &&
                (edgeVertex > 0))
                edgeVertex--;
            vertices.Add(points[edgeVertex]);

            //on 3, creating distance compare base, on higher - comparing
            float lastDrawedEdgeLength = Vector2.Dot((vertices[vertices.Count - 1] - vertices[vertices.Count - 2]), lastEdgeVector.normalized);
            if (vertices.Count == 3)
                correctLengthRelation = lastEdgeVector.magnitude / lastDrawedEdgeLength;
            else if (vertices.Count > (checker.viewer.edges.Count + 1))
                return false; 
            else if (vertices.Count > 3)
                if (!CompareLength(lastDrawedEdgeLength, lastEdgeVector.magnitude))
                    return false;

            //if last edge smaller, than minimum
            if (Vector2.Dot(vertices[vertices.Count - 1] - vertices[vertices.Count - 2], lastEdgeVector.normalized) < checker.minEdgeLenght)
                return false;

            //if new edge start in wrong direction
            if (MinAngleBetween(EdgeVector, points[points.Count - 1] - vertices[vertices.Count - 1]) > checker.maxAngleDifference)
                return false;
        }

        lastPoint = points[points.Count - 1];
        return true;
    }

    public bool IsFinished()
    {
        if (vertices.Count < checker.viewer.edges.Count)
            return false;

        if (vertices.Count == checker.viewer.edges.Count)
        {
            SetNextEdge();
            vertices.Add(lastPoint);
        }

        float dist1 = Vector2.Dot(vertices[1] - vertices[0], EdgeVector.normalized);
        float dist2 = Vector2.Dot(lastPoint - vertices[vertices.Count - 1], EdgeVector.normalized);

        if (!CompareLength(dist1 + dist2, EdgeVector.magnitude))          
            return false;

        if ((lastPoint - vertices[0]).magnitude > checker.maxDistanceBetweenEnds)
            return false;

        return true;
    }

    void SetNextEdge()
    {
        int nextIndex = (currentEdgeIndex + ((directionOfDrawing) ? 1 : -1)) % checker.viewer.edges.Count;
        if (nextIndex < 0)
            nextIndex += checker.viewer.edges.Count;
        currentEdgeIndex = nextIndex;
    }

    bool CompareLength(float drawedLenght, float rightLength)
    { 
        float relation = (drawedLenght * correctLengthRelation) / rightLength;
        if (relation > checker.maxDistanceDeviationUp)
            return false;
        if (relation < (checker.maxDistanceDeviationDown))
            return false;

        return true;
    }

    bool IsPointInCurrentEdge(Vector2 point)
    {
        Vector2 difference = point - vertices[vertices.Count - 1];

        Vector2 vector = EdgeVector;
        vector.Normalize();
        Vector2 normal = new Vector2(-vector.y, vector.x);
        
        Vector2 distance = new Vector2(Vector2.Dot(vector, difference),
            Vector2.Dot(normal, difference));

        float distanceInRightWay = distance.x;
        if (distance.x > 0)
            distance.x = 0;

        return (distance.magnitude > Mathf.Lerp(checker.maxDistanceDevAtStart, checker.maxDistanceDevAtEnd, 
            distanceInRightWay / checker.minDistanceToStopDevGrow));
    }

    float MinAngleBetween(Vector2 a, Vector2 b)
    {
        return Mathf.Min(Vector2.Angle(a, b), Vector2.Angle(b, a));
    }
}
