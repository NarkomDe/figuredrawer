﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LitManagerScript : MonoBehaviour 
{
    public Material unLitMat;
    public Material litMat;
    public Color unLitCol;
    public Color litCol;

    private PatternViewerScript patternViewer;
    private List<int> littedSegments;
    private List<int> littedJoints;

    void Awake()
    {
        patternViewer = GetComponent<PatternViewerScript>();
        littedSegments = new List<int>();
        littedJoints = new List<int>();
    }

    public void LitEdges(List<Match> indices)
    {
        for (int i = 0; i < littedSegments.Count; i++)
            UnLitSegment(littedSegments[i]);
        littedSegments.Clear();

        for (int i = 0; i < littedJoints.Count; i++)
            UnLitJoint(littedJoints[i]);
        littedJoints.Clear();

        for (int i = 0; i < indices.Count; i++)
        {
            int index = indices[i].currentEdgeIndex;
            LitSegment(index);
            LitJoint(index);
            LitJoint((index + 1) % patternViewer.shape.points.Length);
        }
    }

    void LitSegment(int index)
    {
        Transform segment = patternViewer.segments.GetChild(index);
        segment.transform.position = new Vector3(segment.transform.position.x, segment.transform.position.y, 1);
        segment.gameObject.GetComponent<Renderer>().material = litMat;
        littedSegments.Add(index);
    }

    void UnLitSegment(int index)
    {
        Transform segment = patternViewer.segments.GetChild(index);
        segment.transform.position = new Vector3(segment.transform.position.x, segment.transform.position.y, 2);
        segment.gameObject.GetComponent<Renderer>().material = unLitMat;
    }

    void LitJoint(int index)
    {
        if (!littedJoints.Contains(index))
        {
            Transform joint = patternViewer.joints.GetChild(index);
            joint.transform.position = new Vector3(joint.transform.position.x, joint.transform.position.y, 1);
            joint.gameObject.GetComponent<SpriteRenderer>().color = litCol;
            littedJoints.Add(index);
        }
    }

    void UnLitJoint(int index)
    {
        Transform joint = patternViewer.joints.GetChild(index);
        joint.transform.position = new Vector3(joint.transform.position.x, joint.transform.position.y, 2);
        joint.gameObject.GetComponent<SpriteRenderer>().color = unLitCol;
    }
}
