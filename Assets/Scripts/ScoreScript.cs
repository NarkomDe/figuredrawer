﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreScript : MonoBehaviour 
{
    public string baseText = "Score: ";
    private Text textComponent;

    void Awake()
    {
        textComponent = GetComponent<Text>();
    }

    public void SetScore(int score)
    {
        textComponent.text = baseText + score.ToString();
    }
}
