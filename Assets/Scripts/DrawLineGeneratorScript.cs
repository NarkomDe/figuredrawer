﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DrawLineGeneratorScript : MonoBehaviour {
    public GameControllerScript gameController;
    public float minimumOffsetFromLastPoint = .01f;
    public Color rightColor;
    public Color wrongColor;

    [HideInInspector]
    public bool isDrawing;
    private LineRenderer lineRenderer;
    private bool isDrawingRight;
    private List<Vector2> points;
    private PatternCheckerScript patternChecker;

	void Awake() 
	{
        lineRenderer = GetComponent<LineRenderer>();
        isDrawing = false;
        isDrawingRight = true;
        points = new List<Vector2>(1024);
        patternChecker = GetComponent<PatternCheckerScript>();
	}

	void Update() 
	{
        if (isDrawing)
        {
            if (Input.GetMouseButtonUp(0))
                StopDrawing();
            else
            {
                Vector2 newPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                if ((points.Count == 0) || ((newPoint - points[points.Count - 1]).magnitude > minimumOffsetFromLastPoint))
                    AddNewPoint(newPoint);
            }

            SetLineRendererPoints();
        }
        else if (Input.GetMouseButtonDown(0))
            StartDrawing();

	}

    public void Restart()
    {
        patternChecker.Restart();
        isDrawing = false;
        points.Clear();
        SetLineRendererPoints();
    }

    void StartDrawing()
    {
        patternChecker.Restart();
        isDrawing = true;
        isDrawingRight = true;
        lineRenderer.SetColors(rightColor, rightColor);
    }

    void StopDrawing()
    {
        isDrawing = false;
        points.Clear();

        if (patternChecker.IsFinished())
            gameController.NextLevel();

        patternChecker.Restart();
    }

    void AddNewPoint(Vector2 point)
    {
        points.Add(point);
        if (isDrawingRight)
        {
            if (!patternChecker.CheckPattern(points))
            {
                isDrawingRight = false;
                lineRenderer.SetColors(wrongColor, wrongColor);
                patternChecker.Restart();
            }
        }
    }

    //toArray creates garbage, so like this
    void SetLineRendererPoints()
    {
        lineRenderer.SetVertexCount(points.Count);

        for (int i = 0; i < points.Count; i++)
            lineRenderer.SetPosition(i, points[i]);
    }
}
