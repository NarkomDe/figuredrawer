﻿using UnityEngine;
using System.Collections;

public class PrePatternViewerScript : MonoBehaviour {
    public PatternShape shape;

    void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;

        if (shape != null)
            for (int i = 0; i < shape.points.Length; i++)
                Gizmos.DrawLine(shape.points[i], shape.points[(i + 1) % shape.points.Length]);
    }

    [ContextMenu("Generate")]
    void Generate()
    {
        Vector2[] points = new Vector2[10];

        int i = 0;
        for (float f = Mathf.PI / 2; f < Mathf.PI * 2.5f; f += Mathf.PI / 5, i++)
            points[i] = new Vector2(Mathf.Cos(f), Mathf.Sin(f)) * ((i % 2 == 1) ? 1.2f : 3.5f);

        shape.points = points;
    }
}
