﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TimerScript : MonoBehaviour 
{
    public GameControllerScript gameController;
    public string format = "##.#";

    private float timeLeft;
    private Text textComponent;

    void Awake()
    {
        textComponent = GetComponent<Text>();
    }

    public void SetTimer(float time)
    {
        timeLeft = time;
    }

    void Update()
    {
        timeLeft -= Time.deltaTime;

        if (timeLeft <= 0)
            gameController.EndGame();

        textComponent.text = timeLeft.ToString(format);
    }
}
